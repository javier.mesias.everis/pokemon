Pokemon's Code Exercise
========================

Requirements
---------------
php7.1 or higher.

Getting Started
---------------

Clone this repository :-

```
git clone https://gitlab.com/javier.mesias.everis/pokemon.git
```

Run Composer or unzip file vendor.zip :-

```
composer install
```

Create .env file  :-

```
cp .env.example .env
```

Generate key  :-

```
php artisan key:generate
```

Check permissions in storage folder  :-

```
 chmod -R 777 storage
```


Start server in port 8001  :-

```
 php artisan serve --port=8001
```


For running tests, run :-

```
./vendor/bin/phpunit
```

## Captura

![alt text](https://gitlab.com/javier.mesias.everis/pokemon/raw/master/captura.png)