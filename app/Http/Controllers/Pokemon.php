<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Helper\PokeHelper;

class Pokemon extends Controller
{
    
    public function search(Request $request)
    {
            $pokeHelper = new PokeHelper();
            $q = $request->get('q');
            $pokemonArray = explode(',' , $q);
            $pokemonColletion = $pokeHelper->getPokemon($pokemonArray);

            header('Access-Control-Allow-Origin: *');
            return response()->json(array(
                        'data'      => $pokemonColletion['data']
                    ), $pokemonColletion['code']); 
    }

}
