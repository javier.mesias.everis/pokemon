<?php

namespace App\Helper;

class PokeHelper 
{

    private $pokemonColletion = [];
    private $data;
    
    public function __construct()
    {
    
    }

    private function setPokemonColletion($index)
    {
       $this->pokemonColletion[$index]['name'] = $this->name();
       $this->pokemonColletion[$index]['weight'] = $this->weight();
       $this->pokemonColletion[$index]['height'] = $this->height();
       $this->pokemonColletion[$index]['sprites'] = $this->sprites();
       $this->pokemonColletion[$index]['abilities'] = $this->abilities();
       $this->pokemonColletion[$index]['base_experience'] = $this->baseExperience();
       $this->pokemonColletion[$index]['forms'] = $this->forms();
       $this->pokemonColletion[$index]['held_items'] = $this->heldItems();
       $this->pokemonColletion[$index]['moves'] = $this->moves();
       $this->pokemonColletion[$index]['species'] = $this->species();
       $this->pokemonColletion[$index]['types'] = $this->types();  
    }


    public function callPokeApi($url)
    {
        $response = GuzzleHelper::getGuzzleRequest($url);
        $code = $response['code'];

        if ($code == 200){
              $this->data = json_decode($response['data']); 
        }
        
        return $code;
    }


    public function getPokemon($pokemonArray)
    {
        $foundPokemon = false;
        $index = 0;
        foreach ($pokemonArray as $key => $value) {
            $url = 'https://pokeapi.co/api/v2/pokemon/'. strtolower($value).'/';
            $code = $this->callPokeApi($url);
            if ($code == 200){
                $foundPokemon = true;     
                $this->setPokemonColletion($index);
                 $index++;
            }
            
        }

        if ($foundPokemon){
            $code = 200;
        }
       
        return ['code'=>$code, 'data'=> $this->pokemonColletion ];
    }



    private function name()
    {
        return $this->data->name;
    }

    private function sprites()
    {
        return $this->data->sprites;
    }

    private function height()
    {
        return $this->data->height;
    }

    private function weight()
    {
        return $this->data->weight;
    }

    private function abilities()
    {
        $abilities = [];

        foreach ($this->data->abilities as $key => $value) {
                $abilitie['name'] = $value->ability->name;
                $abilitie['slot'] = $value->slot;
                $abilities[] = $abilitie;
        }

        return $abilities;
    }

    private function heldItems()
    {
        $heldItems = [];

        foreach ($this->data->held_items as $key => $value) {
                $heldItem['name'] = $value->item->name;
                $heldItems[] = $heldItem;
        }

        return $heldItems;
    }

    

    private function baseExperience()
    {
        return $this->data->base_experience;
    }

    private function forms()
    {
        $forms = [];

        foreach ($this->data->forms as $key => $value) {
                $form['name'] = $value->name;
                $forms[] = $form;
        }

        return $forms;
    }

    private function species()
    {
        return $this->data->species->name;
    }

    private function types()
    {
        $types = [];

        foreach ($this->data->types as $key => $value) {
                $type['slot'] = $value->slot;
                $type['type'] = $value->type->name;
                $types[] = $type;
        }

        return $types;
    }

    

    private function Moves()
    {
        $moves = [];

        foreach ($this->data->moves as $key => $value) {
                $move['name'] = $value->move->name;
                $moves[] = $move;

                if ($key == 4) break;
        }

        return $moves;
    }
    

}

?>