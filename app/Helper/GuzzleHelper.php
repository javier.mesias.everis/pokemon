<?php

namespace App\Helper;

class GuzzleHelper 
{

	public static function getGuzzleRequest($url)
    {
    	$code = 0; 
    	$data = [];
        $client = new \GuzzleHttp\Client();
        $request = $client->get($url , ['http_errors' => false]);
        $response = $request->getBody();
        $code = $request->getStatusCode();

        if ($code == 200){
              $data = $response; 
        }
        
        return ['code'=>$code , 'data'=>$data];
    }

}


?>