<?php 

use PHPUnit\Framework\TestCase;
use App\Helper\PokeHelper;

class PokemonTest  extends TestCase { 
    
    private $pokeHelper;
  
    public function setUp()
    {
      	$this->pokeHelper = new PokeHelper();
    }


    public function testCodeOk()
    {

    	
        $pokemonArray = ['squirtle'];
        $pokemonColletion = $this->pokeHelper->getPokemon($pokemonArray);

        $this->assertEquals($pokemonColletion['code'],200);
    }

     public function testCodeFail()
    {

        
        $pokemonArray = ['nnn'];
        $pokemonColletion = $this->pokeHelper->getPokemon($pokemonArray);

        $this->assertEquals($pokemonColletion['code'],404);
    }

    public function testPokemonName()
    {

    	$pokeHelper = new PokeHelper();
        $pokemonArray = ['Pikachu','Squirtle'];
        $pokemonColletion = $this->pokeHelper->getPokemon($pokemonArray);
        
        $this->assertEquals($pokemonColletion['data'][0]['name'],'pikachu');
        $this->assertEquals($pokemonColletion['data'][1]['name'],'squirtle');
    }

    public function testCheckArrays()
    {
        $pokemonArray = ['Jolteon'];
        $pokemonColletion = $this->pokeHelper->getPokemon($pokemonArray);
 
        $this->assertTrue(is_array($pokemonColletion));
        $this->assertArrayHasKey('held_items',$pokemonColletion['data'][0]);
        $this->assertArrayHasKey('held_items',$pokemonColletion['data'][0]);
        $this->assertArrayHasKey('abilities',$pokemonColletion['data'][0]);
        $this->assertArrayHasKey('moves',$pokemonColletion['data'][0]);
        $this->assertArrayHasKey('types',$pokemonColletion['data'][0]);
    }



} 