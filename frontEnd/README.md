Pokemon's Code Exercise
========================

## Installation

Clone repo :-

```
git clone https://gitlab.com/javier.mesias.everis/pokemon.git
```

Enter frontEnd directory :-

```
cd frontEnd/ 
```

Run npm install or unzip node_modules.zip :-

```
npm install
```

Start server in http://localhost:3000/

```
npm start
``` 

