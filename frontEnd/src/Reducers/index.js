import constants from "../Constants";

const initialState = {
            pokemonList:null
}


const mainReducer=(state=initialState,action)=>{


    if(action.type === constants.LOAD_POKEMON_LIST){
        return{
            ...state,
            pokemonList:action.data.data.data
        }
    }if(action.type === constants.LOAD_POKEMON_LOADING){
        return{
            ...state,
            pokemonList:null
        }
    }if(action.type === constants.LOAD_POKEMON_ERROR){
        return{
            ...state,
            pokemonList:-1
        }
    }else{
        return{
            ...state,
            initialState
        }
    }



}

export default mainReducer;