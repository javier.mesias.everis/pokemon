import axios from "axios";
import constants from "../Constants";

export function loadPokemonList(tags){
   
    return(dispatch)=>{
        dispatch(Loading(null));
        let pokemonNamesArray;
        pokemonNamesArray = tags.map((pokemon,i) => (pokemon.text)).join(',');

        return axios.get(constants.URL_POKEMON_LIST + '?q=' + pokemonNamesArray ).then((response)=>{
            dispatch(setPokemonList(response));
        }).catch(error => {
		    dispatch(setPokemonError(error));
		});
    }
}

export function setPokemonList(data){
	
    return{
        type:constants.LOAD_POKEMON_LIST,
        data
    }
}


export function Loading(data){
    
    return{
        type:constants.LOAD_POKEMON_LOADING,
        data
    }
}


export function setPokemonError(data){
    
    return{
        type:constants.LOAD_POKEMON_ERROR,
        data
    }
}



