import React, { Component , Fragment } from 'react';
import Header from './Components/layout/Header';
import Footer from './Components/layout/Footer';


class App extends Component {
  render() {
    return (
       <Fragment>
       	    <div className="container">
	       		<Header />
	       		<Footer/>
       		</div>
       </Fragment>
    );
  }
}

export default App;
