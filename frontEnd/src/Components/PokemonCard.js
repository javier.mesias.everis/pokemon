import React , {  Fragment  } from 'react';


const PokemonCard = ({ pokemon} ) => {
   
    return (
      <Fragment>
        
            <div className="card mb-3 col-md-12" >
              <div className="row no-gutters">
                <div className="col-md-4">
                  {  (pokemon.sprites.front_default != null ) ? <img title="front default" src={pokemon.sprites.front_default}  alt={pokemon.name} /> : ''   } 
                  {  (pokemon.sprites.front_female != null ) ? <img title="front female" src={pokemon.sprites.front_female}  alt={pokemon.name} /> : ''   } 
                  {  (pokemon.sprites.front_shiny != null ) ? <img title="front shiny" src={pokemon.sprites.front_shiny}  alt={pokemon.name} /> : ''   } 
                  {  (pokemon.sprites.front_shiny_female != null ) ? <img title="front shiny female"  src={pokemon.sprites.front_shiny_female}  alt={pokemon.name} /> : ''   } 
                  {  (pokemon.sprites.back_default != null ) ? <img title="back default" src={pokemon.sprites.back_default}  alt={pokemon.name} /> : ''   } 
                  {  (pokemon.sprites.back_female != null ) ? <img title="back female" src={pokemon.sprites.back_female}  alt={pokemon.name} /> : ''   } 
                  {  (pokemon.sprites.back_shiny != null ) ? <img title="back shiny" src={pokemon.sprites.back_shiny}  alt={pokemon.name} /> : ''   } 
                  {  (pokemon.sprites.back_shiny_female != null ) ? <img title="back shiny female" src={pokemon.sprites.back_shiny_female}  alt={pokemon.name} /> : ''   } 
                </div>
                  <div className="col-md-8">
                  <div className="card-body">
                  <p  style={ { fontSize: '32px' } } ><span className="badge  badge-warning" style={ { padding: '20px' , fontFamily: 'pokemon-font'  ,border: '8px solid #3267B0' , color: '#3267B0' , background: '#FFCB05' } }>{pokemon.name}</span></p>
                  
                  <p className="card-text ">
                    <span className="card-text mr-2"><b>Base Experience:</b> {pokemon.base_experience}</span>
                  </p>
                  <p className="card-text ">
                    <span className="card-text mr-2"><b>Weight:</b> {pokemon.weight}</span>
                    <span className="card-text mr-2"><b>Height:</b> {pokemon.height}</span>
                  </p>

                  <p className="card-text ">
                    {
                      (pokemon.abilities.length > 0 ) ? 
                     <span className="card-text mr-2"><b>Abilities:</b></span> : ''
                    }
                    {
                          pokemon.abilities.map((abiliti,i) => {
                              return (
                                  <span key={i} style={ { fontSize: '14px' } }  className="badge badge-pill badge-danger mr-2">{abiliti.name}</span>
                              ) 
                          })
                     }
                  </p>

                  <p className="card-text ">
                    {
                      (pokemon.types.length > 0 ) ? 
                     <span className="card-text mr-2"><b>Types:</b></span> : ''
                    }
                    {
                          pokemon.types.map((oType,i) => {
                              return (
                                  <span key={i} style={ { fontSize: '14px' } }  className="badge badge-pill badge-primary mr-2">{oType.type}</span>
                              ) 
                          })
                     }
                  </p>


                  <p className="card-text ">
                    {
                      (pokemon.moves.length > 0 ) ? 
                     <span className="card-text mr-2"><b>Moves:</b></span> : ''
                    }
                    {
                          pokemon.moves.map((move,i) => {
                              return (
                                  <span key={i} style={ { fontSize: '14px' } }  className="badge badge-pill badge-success mr-2">{move.name}</span>
                              ) 
                          })
                     }
                  </p>
                  <p className="card-text ">
                    {
                      (pokemon.held_items.length > 0 ) ? 
                     <span className="card-text mr-2"><b>Held Items:</b></span> : ''
                    }
                      {
                            pokemon.held_items.map((held,i) => {
                                return (
                                    <span key={i} style={ { fontSize: '14px' } }  className="badge badge-pill badge-info mr-2">{held.name}</span>
                                ) 
                            })
                       }

                   
                  </p>
                 
                  </div>
                </div>
              </div>
            </div>

      </Fragment>
    );
}

export default PokemonCard;