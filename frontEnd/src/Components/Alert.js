import React from 'react';

const Alert = ({ message }) => {
		return (
			<div className="alert alert-warning" role="alert">
				<h1 className="alert-heading">Opps!  </h1>
				<h4>{message}</h4>
			</div>
		);
}

export default Alert;