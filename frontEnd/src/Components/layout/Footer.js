import React  from 'react';
import Logo from "./images/logo.png";
const Footer = () => {
    return (
            <div className="col-md-12 bg-primary mt-3 pt-1 pb-3" >
              <div className="container">
                <p className="text-light h3 mt-5"><img src={Logo} alt="logo" width="150px" /></p>
                <p className="text-light">Hecho por <b>Javier Mesías</b>
                	 <span className="text-light" onClick={ (e) => { window.location.href='https://gitlab.com/javier.mesias.everis/pokemon'  } }  style={{ cursor: 'pointer' }}> <b>Link a mi Repo</b></span>
                </p>
               
              </div>
            </div>
    );
}

export default Footer;