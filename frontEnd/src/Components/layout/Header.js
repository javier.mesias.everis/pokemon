import React , { Fragment } from 'react';
import Logo from "./images/logo.png";
import SearchPokemon from './SearchPokemon';


const Header = () => {
                   
            return (
                <Fragment>
                  <nav className="navbar navbar-expand-lg navbar-dark bg-primary justify-content-between d-flex mb-4">
                      <div className="container">
                        
                          <p className="text-light h3 mr-2 w-100"><img src={Logo} alt="logo" width="300px" /><b className="text-warning" style={{fontFamily: 'pokemon-font'}}>finder</b></p>
                           <div className="ml-4">
                           
                            </div>
                          <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navegacion" aria-controls="navegacion" aria-expanded="false" aria-label="Toggle navigation">
                              <span className="navbar-toggler-icon"></span>
                          </button>

                          <div className="collapse navbar-collapse" id="navegacion">

                              <ul className="navbar-nav ml-auto text-right">
                                  <li className="nav-item dropdown mr-md-2 mb-2 mb-md-0">
                                          <div className="" aria-labelledby="navegacion">
                                          </div>
                                  </li>
                                  <li className="nav-item dropdown mr-md-2 mb-2 mb-md-0" >
                                                 
                                  </li>

                              </ul>
                          </div>
                      </div>
                  </nav>
                  <div className="row">
                      <div className="col-md-12">
                          <div className="mb-3 ">
                                <SearchPokemon   />
                          </div>
                      </div>
                       
                  </div>
                </Fragment>
            );
    
}



export default Header;