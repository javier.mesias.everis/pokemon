import React , {Component} from 'react';
import {connect} from "react-redux";
import { WithContext as ReactTags } from 'react-tag-input';
import {loadPokemonList} from "../../Actions/index.js"
import PokemonList from '../PokemonList';
import Loading from '../Loading';
import poke_names from '../../Functions/PokemonSuggestions';

const KeyCodes = {
  comma: 188,
  enter: 13
};

const delimiters = [KeyCodes.comma, KeyCodes.enter];

class SearchPokemon extends Component {
    
    state = {  }    

    constructor (props) {
        super(props);

        this.state = {
            tags: [
                { id: "Pikachu", text: "Pikachu" },
                { id: "Squirtle", text: "Squirtle" }
             ],
            suggestions:  poke_names.suggestions
        };
        this.handleDelete = this.handleDelete.bind(this);
        this.handleAddition = this.handleAddition.bind(this);
        this.handleDrag = this.handleDrag.bind(this);
        
        this.props.loadPokemonList(this.state.tags)

    }
    
    handleDelete(i) {
        const { tags } = this.state;
        this.setState({
         tags: tags.filter((tag, index) => index !== i),
        });
    }
 
    handleAddition(tag) {
        this.setState(state => ({ tags: [...state.tags, tag] }), () => { this.props.loadPokemonList(this.state.tags) });
    }

    handleDrag(tag, currPos, newPos) {
        const tags = [...this.state.tags];
        const newTags = tags.slice();
 
        newTags.splice(currPos, 1);
        newTags.splice(newPos, 0, tag);
 
        // re-render
        this.setState({ tags: newTags });
    }

    findPokemonByName = (e) => {
        let pokemonName = document.getElementById('name-pokemon').value;
        let tag = {id:pokemonName, text: pokemonName}
        if (pokemonName.trim().length === 0){
            this.props.loadPokemonList(this.state.tags);
        }else {
             this.handleAddition(tag);
        }
       
        
         
    }
 
      render() {
        //console.log(this.props.pokemonList);
        const { tags, suggestions } = this.state;
        return (
            <div>
                <div className="col-md-12">
                    <div className="input-group mb-3">
                        <ReactTags 
                            id={'name-pokemon'}
                            tags={tags}
                            suggestions={suggestions}
                            handleDelete={this.handleDelete}
                            handleAddition={this.handleAddition}
                            delimiters={delimiters}
                            handleDrag={this.handleDrag}
                            placeholder={' Add pokemon'}
                             />

                          <span className="input-group-btn mt-2">
                                <button type="button" style={ {  border: '5px solid #3267B0' , fontWeight: 'bold' , color: '#3267B0' , background: '#FFCB05' } } onClick={ (e) => { this.findPokemonByName(e)  } }  className="btn btn-success"><i className="fa fa-search mr-2"></i>Find</button>
                          </span> 
                    </div>
                </div>
                { 

                     (this.props.pokemonList === null ) ? 
                    <Loading  type={'Grid'} color={'#0757A8'} width={200} height= {200} />   :  
                    <PokemonList data={this.props.pokemonList}  />   

                }
            </div>
        )
              
    }

    
}

const mapStateToProps=(state) =>{
    return { pokemonList:state.pokemonList  }
};

export default connect (mapStateToProps, {loadPokemonList})(SearchPokemon);