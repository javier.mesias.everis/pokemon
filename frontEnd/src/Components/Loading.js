import React from 'react';
import Loader from 'react-loader-spinner'
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css"

const Loading = ({ type, color, width , height }) => {
	return ( 
		<center>
		<Loader
         type={type}
         color={color}
         height={height}
         width={width}
       />
       </center>

	);
   
}
 
export default Loading;