import React , {  Fragment  } from 'react';
import PokemonCard from './PokemonCard';
import Alert from './Alert';

const PokemonList = ({ data} ) => {
 
    return (
      <Fragment>
        {
          (data === -1) ? <Alert message={'Could not find pokemon Try again !!'} /> :  
             (data.length > 0) ?
              data.map((pokemon,i) => {
                  return (
                      <div key={i} className="col-md-12" >
                            <div className="row">
                            <PokemonCard  pokemon={pokemon}  />
                            </div>
                      </div>
                  ) 
              })  : ''
         }
        

      </Fragment>
    );
}

export default PokemonList;